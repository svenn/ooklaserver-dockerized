###################################
# Dockerfile for OoklaServer      #
#        using nginx              #
###################################

FROM debian:stretch
MAINTAINER Sven Masuhr <mail@svenn.me>

RUN mkdir -p /data /run/php
WORKDIR /data

RUN groupadd -r speedtest && useradd -r -g speedtest speedtest
RUN apt-get update && \
    apt-get -y install wget ca-certificates gnupg apt-transport-https apt-utils unzip
ADD assets/php7.3.list /etc/apt/sources.list.d/php7.3.list
RUN wget -q https://packages.sury.org/php/apt.gpg -O - | apt-key add -
RUN apt-get update && \
    apt-get -y install nginx nginx-extras php7.3-fpm
RUN wget http://install.speedtest.net/httplegacy/http_legacy_fallback.zip && \
    unzip http_legacy_fallback.zip -d /var/www/html/ && \
    rm http_legacy_fallback.zip /var/www/html/index.nginx-debian.html
RUN wget http://install.speedtest.net/ooklaserver/ooklaserver.sh && \
    chmod a+x ooklaserver.sh && \
    ./ooklaserver.sh install -f
RUN chown -R speedtest:speedtest /data
RUN echo "" > /var/www/html/speedtest/index.html
RUN apt-get clean

ADD assets/crossdomain.xml /var/www/html/speedtest
ADD assets/entrypoint.sh /usr/bin/entrypoint.sh
ADD assets/nginx.conf /etc/nginx/nginx.conf

ENTRYPOINT ["/bin/sh", "/usr/bin/entrypoint.sh"]

EXPOSE 8080 80 5060
